export class ProductoModel{//modelo del producto
    id:string='';
    nombreProducto:string='';
    imagen:string='';
    marca:string='';
    stock:number=0;
    precio:number=0;
    descripcion:string='';
}