import { Directive, HostBinding, HostListener } from "@angular/core";
  //Directiva para la apertura del dropdown
@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective{
    @HostBinding('class.open') isOpen=false;
    //mantiene el dropdown cerrado
    @HostListener('click') toggleOpen(){
        this.isOpen=!this.isOpen;
    //abre el dropdown al escuchar un click en el botón
    }
}