import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";


@Injectable({
    providedIn:'root'
})
export class ApiService{
 
    constructor(private http:HttpClient){
        
    }

    postProduct(data:any){ //subir productos
        return this.http.post<any>('http://localhost:3000/productos',data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }
    getProduct(){//obtener productos
        return this.http.get<any>('http://localhost:3000/productos')
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    editProducts(data:any,id:string){//editar productos
        return this.http.put<any>('http://localhost:3000/productos/'+id,data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    updateProducts(data:any,id:string){//actualizar productos
        return this.http.put<any>('http://localhost:3000/productos/'+id,data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    deleteProducts(id:string){//borrar productos
        return this.http.delete<any>('http://localhost:3000/productos/'+id)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    postUsers(data:any){
        return this.http.post<any>('http://localhost:3000/usuarios',data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    getUser(){//Obtener usuario
       return this.http.get<any>('http://localhost:3000/usuarios')
       .pipe(map((res:any)=>{
        return res;
       }))
    }
    editUsers(data:any,id:number){//editar usuario
        return this.http.put<any>('http://localhost:3000/usuarios/'+id,data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    updateUsers(data:any,id:number){//actualizar usuario
        return this.http.put<any>('http://localhost:3000/productos/'+id,data)
        .pipe(map((res:any)=>{
            return res;
        }))
    }

    deleteUsers(id:number){//borrar usuario
        return this.http.delete<any>('http://localhost:3000/usuarios/'+id)
        .pipe(map((res:any)=>{
            return res;
        }))
    }


}