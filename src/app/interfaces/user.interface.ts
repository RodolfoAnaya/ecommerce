export interface Usuarios {
    id:      number;
    perfil:  string;
    usuario: string;
    email:   string;
    pass:    string;
}
