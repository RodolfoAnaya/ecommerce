import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AdmninServiceService } from '../services/admnin-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() featureSelected=new EventEmitter<string>();

  adminUser:any; //Variable para saber si el usuario es admin

  constructor(private dataUserService:AdmninServiceService) { }
  
  ngOnInit(){
    this.adminUser=this.dataUserService.onAdmin;
  }


}
