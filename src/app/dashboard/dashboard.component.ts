import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../shared/api.service';


import { Usuarios } from '../interfaces/user.interface';
import { ProductoModel } from '../shared/producto.model';
import { UserService } from '../services/user.service';
import { AdmninServiceService } from '../services/admnin-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  @Input() usuarios:Usuarios[]=[];

  adminUser!:boolean;
  formValue!:FormGroup;
  productModel:ProductoModel=new ProductoModel();
  productData!:any;
  showAdd!:boolean;
  showUpdate!:boolean;
  dataUser: any;

  constructor(
    private formBuilder:FormBuilder, 
    private api:ApiService,
    private dataUserService:AdmninServiceService,
    ) { }
  
  ngOnInit(): void {
    this.formValue=this.formBuilder.group({
      id:[''],
      nombreProducto:[''],
      imagen:[''],
      marca:[''],
      stock:[''],
      precio:[''],
      descripcion:['']
    })
    this.getAllProducts();//obtener todos los productos constantemente
    this.dataUser=this.dataUserService.onAdmin //Obtener la ifno de usuario constantemente

  }

  clickAddProduct(){ //funcion para agrgegar producto
    this.formValue.reset();
    this.showAdd=true;
    this.showUpdate=false;
  }

  postProductoDetail(){ //Colocar el detalle del producto cuando se presiona el boton agregar
    this.productModel.id=this.formValue.value.id;
    this.productModel.nombreProducto=this.formValue.value.nombreProducto;
    this.productModel.imagen=this.formValue.value.imagen;
    this.productModel.marca=this.formValue.value.marca;
    this.productModel.stock=this.formValue.value.stock;
    this.productModel.precio=this.formValue.value.precio;
    this.productModel.descripcion=this.formValue.value.descripcion;
    this.api.postProduct(this.productModel)
    .subscribe(resp=>{
      console.log(resp);
      alert("Producto agregado correctamente")
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllProducts();
    }, error=>{
      alert("Error al agregar el producto")
    });
  }

  getAllProducts(){//funcion para obtener los productos
    this.api.getProduct()
    .subscribe(resp=>{
      this.productData=resp;
    })
  }

  deleteProduct(product:any){//funvion para borrar productos
    this.api.deleteProducts(product.id)
    .subscribe(resp=>{
      alert("Articulo eliminado con exito")
      this.getAllProducts();
    });
  }

  onEditProduct(product:any){// funcion para editar el articulo
    this.showAdd=false; 
    this.showUpdate=true;
    this.productModel.id=product.id;
    this.formValue.controls['id'].setValue(product.id)
    this.formValue.controls['nombreProducto'].setValue(product.nombreProducto)
    this.formValue.controls['imagen'].setValue(product.imagen)
    this.formValue.controls['marca'].setValue(product.marca)
    this.formValue.controls['stock'].setValue(product.stock)
    this.formValue.controls['precio'].setValue(product.precio)
    this.formValue.controls['descripcion'].setValue(product.descripcion)
  }

  updateProductoDetail(){ //funcion para actualizar el articulo
    this.productModel.id=this.formValue.value.id;
    this.productModel.nombreProducto=this.formValue.value.nombreProducto;
    this.productModel.imagen=this.formValue.value.imagen;
    this.productModel.marca=this.formValue.value.marca;
    this.productModel.stock=this.formValue.value.stock;
    this.productModel.precio=this.formValue.value.precio;
    this.productModel.descripcion=this.formValue.value.descripcion;
    this.api.updateProducts(this.productModel, this.productModel.id)
    .subscribe(resp=>{
      alert("Producto actualizado")
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllProducts();
    })
  }


}
