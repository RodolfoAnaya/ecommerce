import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'
import { AdmninServiceService } from 'src/app/services/admnin-service.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
    public loginForm!:FormGroup;

    constructor(
        private formBuilder:FormBuilder, 
        private http:HttpClient,
        private router:Router,
        private admin:AdmninServiceService){}

    login(){
        this.http.get<any>("http://localhost:3000/usuarios")
        .subscribe(resp=>{
            const user=resp.find((a:any)=>{
                return a.email===this.loginForm.value.email 
                && a.pass===this.loginForm.value.pass
            
            });
            if(user){
                this.admin.onAdmin=user;
                //console.log(this.admin.onAdmin)
                alert("Inicio de sesion correcto");
                this.loginForm.reset();
                this.router.navigate(['dashboard']);
            }else{
                alert("Usuario, contraseña o perfil incorrecta")
            }
        }, error=>{
            alert("Algo salio mal")
        })
    }

    ngOnInit(): void {
        this.loginForm=this.formBuilder.group({
            email:['',Validators.required],
            pass:['',Validators.required],
            usuario:[''],
            perfil:['']
        })
    }

    onLogin(form:NgForm){
        const usuario=form.value.usuario
        const email=form.value.email
        const pass=form.value.pass
        const perfil=form.value.perfil
    }

}
