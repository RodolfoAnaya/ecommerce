import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ApiService } from 'src/app/shared/api.service';
import { UserModel } from 'src/app/shared/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  formValue!:FormGroup;
  userModel:UserModel=new UserModel();
  adminUser!:boolean;
  userData!:any;
  user!:any;
  showAdd!:boolean;
  showUpdate:boolean;

  constructor(private formBuilder:FormBuilder, 
    private api:ApiService) { }



  ngOnInit(): void {
    this.formValue=this.formBuilder.group({
      id:[''],
      perfil:[''],
      usuario:[''],
      email:[''],
      pass:['']
    })
    this.getAllUsers();
  }

  clickAddUser(){//funcion para agregar usuario
    this.formValue.reset();
    this.showAdd=true;
    this.showUpdate=false;
  }

  postUserDetail(){ //funcion para subir usuario
    this.userModel.id=this.formValue.value.id;
    this.userModel.perfil=this.formValue.value.perfil;
    this.userModel.usuario=this.formValue.value.usuario;
    this.userModel.email=this.formValue.value.email;
    this.userModel.pass=this.formValue.value.pass;
    this.api.postUsers(this.userModel)
    .subscribe(resp=>{
      console.log(resp);
      alert("Usuario agregado correctamente")
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllUsers();
    }, error=>{
      alert("Error al dar de alta al usuario")
    });
  }

  getAllUsers(){//funcion para obtener todos los empleaodos
    this.api.getUser()
    .subscribe(resp=>{
      this.userData=resp;
    })
  }

  deleteUser(user:any){//funcion para borrar empleados
    this.api.deleteUsers(user.id)
    .subscribe(resp=>{
      alert("Usuario eliminado con exito")
      this.getAllUsers();
    });
  }

  
  onEditUser(user:any){//funcion para editar usuarios
    this.showAdd=false;
    this.showUpdate=true;
    this.userModel.id=user.id;
    this.formValue.controls['id'].setValue(user.id)
    this.formValue.controls['perfil'].setValue(user.perfil)
    this.formValue.controls['usuario'].setValue(user.usuario)
    this.formValue.controls['email'].setValue(user.email)
    this.formValue.controls['pass'].setValue(user.pass)
  }

  updateUserDetail(){ //funcion para actualizar usuarios
    this.userModel.id=this.formValue.value.id;
    this.userModel.perfil=this.formValue.value.perfil;
    this.userModel.usuario=this.formValue.value.usuario;
    this.userModel.email=this.formValue.value.email;
    this.userModel.pass=this.formValue.value.pass;
    this.api.updateUsers(this.userModel, this.userModel.id)
    .subscribe(resp=>{
      alert("Usuario actualizado")
      let ref=document.getElementById('cancel')
      ref?.click();
      this.formValue.reset();
      this.getAllUsers();
    })
  }

}
